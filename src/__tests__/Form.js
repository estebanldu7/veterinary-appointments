import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Formulario from '../components/Formulario';
import '@testing-library/jest-dom/extend-expect';

const crearCita = jest.fn();    

test('<Form /> Load form and check all is correct', () => {

    render(
        <Formulario 
            crearCita={crearCita}
        /> 
    );

    expect(screen.getByText('Crear Cita')).toBeInTheDocument();

    //Heading
    const title = screen.getByTestId('title');
    expect(title.textContent).toBe('Crear Cita');
    expect(title.tagName).not.toBe('H1');

    //Submit button
    const button = screen.getByTestId('btn-submit');
    expect(button.tagName).toBe('BUTTON');
    expect(button.textContent).toBe('Agregar Cita');

});

test('<Formulario /> Form Validation', () => {
    //Mount Form component

    render(
        <Formulario 
            crearCita={crearCita}
        /> 
    );

    userEvent.type(screen.getByTestId('mascota'), 'Coco'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('propietario'), 'Esteban'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('fecha'), '2020-10-31'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('hora'), '19:30'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('sintomas'), 'Estornuda mucho'); //Element where will writing, and string

    //click on button
    const btnSubmit = screen.getByTestId('btn-submit');
    userEvent.click(btnSubmit);

    //Check Alert
    const alert = screen.queryByTestId('alerta'); //Conditional query maybe exist or not
    expect(alert).not.toBeInTheDocument();

    //Create meeting
    expect(crearCita).toHaveBeenCalled();       
    expect(crearCita).toHaveBeenCalledTimes(1); //Is called once       

});