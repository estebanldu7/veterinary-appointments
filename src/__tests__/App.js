import React from 'react';
import {render, screen} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../App';
import '@testing-library/jest-dom/extend-expect';

test('<App/> Application works', () => {
    // check if element is mounted
    // const wrapper =  render(<App />);
    // wrapper.debug();

    render(<App />);
    expect(screen.getByText('Administrador de Pacientes')).toBeInTheDocument(); //check if title is showed
    expect(screen.getByTestId('nombre-app').textContent).toBe('Administrador de Pacientes'); //check if title is showed

    expect(screen.getByText('Crear Cita')).toBeInTheDocument(); //check if title is showed
    expect(screen.getByText('No hay citas')).toBeInTheDocument(); //check if title is showed
});

test('<App/> Check heading and add meeting', () => {

    render(<App />);

    userEvent.type(screen.getByTestId('mascota'), 'Coco'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('propietario'), 'Esteban'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('fecha'), '2020-10-31'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('hora'), '19:30'); //Element where will writing, and string
    userEvent.type(screen.getByTestId('sintomas'), 'Estornuda mucho'); //Element where will writing, and string

    //click on button
    const btnSubmit = screen.getByTestId('btn-submit');
    userEvent.click(btnSubmit);

    //Check Alert
    const alert = screen.queryByTestId('alerta'); //Conditional query maybe exist or not
    expect(alert).not.toBeInTheDocument();

    const dynamicTitle = screen.getByTestId('titulo-dinamico');
    expect(dynamicTitle.textContent).toBe('Administra tus Citas');
    expect(dynamicTitle.textContent).not.toBe('No hya citas');
    expect(dynamicTitle.tagName).toBe('H2');
});

test('<App/> Check booking on DOM', async () => {
    render(<App />);

    const booking = await screen.findAllByTestId('cita');

    expect(screen.getByTestId('btn-eliminar').tagName).toBe('BUTTON');
    expect(screen.getByText('Coco')).toBeInTheDocument();

    //create snap file for check content
    // expect(booking).toMatchSnapshot();
});

test('<App/> Delete booking', () => {

    render(<App />);

    const btnDelete = screen.getByTestId('btn-eliminar');   
    expect(btnDelete.tagName).toBe('BUTTON');
    expect(btnDelete).toBeInTheDocument();

    //Click on delete button
    userEvent.click(btnDelete);

    //Button shouldn't exist
    expect(btnDelete).not.toBeInTheDocument();

    //Meeting shouldn't exist
    expect(screen.queryByText('Coco')).not.toBeInTheDocument();


});